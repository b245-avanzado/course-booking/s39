const mongoose = require("mongoose");

const coursesSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Course name is required!"]
	},
	description: {
		type: String,
		required: [true, "Course description is required!"]
	},
	price: {
		type: Number,
		required: [true, "Price of the course is required!"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	dateCreated: {
		type: Date,
		// The "new Date()" expression instantiates a new "date" that stores the current date and time whenever a course is created in our database.
		default: new Date()
	},
	enrollees: [
		{
			userId: {
				type: String,
				required: [true, "UserId is required!"]
			},
			dateEnrolled: {
				type: Date,
				default: new Date()
			}
		}
	]
})


module.exports = mongoose.model("Course", coursesSchema);