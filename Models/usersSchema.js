const mongoose = require("mongoose");

const usersSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "Please input your first name."]
	},
	lastName: {
		type: String,
		required: [true, "Please input your last name."]
	},
	email: {
		type: String,
		required: [true, "Email address is required!"]
	},
	password: {
		type: String,
		required: [true, "Password is required!"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Please input your contact number"]
	},
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "Course Id is required!"]
			},
			dateEnrolled: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Enrolled"
			}
		}
	]
})

module.exports = mongoose.model("User", usersSchema)