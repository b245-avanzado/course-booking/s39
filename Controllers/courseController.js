const Course = require("../Models/coursesSchema");
const auth = require("../auth.js");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new Course to the database
*/

module.exports.addCourse = (request, response) => {
	let input = request.body;

	let newCourse = new Course({
		name: input.name,
		description: input.description,
		price: input.price
	});

	const userData = auth.decode(request.headers.authorization);

	Course.findOne({name: input.name})
	// saves the created object to our database
	.then(result => {
		if (userData.isAdmin) {
			if (result !== null) {
				return response.send(`The course ${input.name} already exists!`);
			} else {
				newCourse.save();
				return response.send(`You have created a new course name ${input.name}!`);
			}
		} else {
			return response.send("You do not have permission to create a course.");
		}
	})
	// course creation failed
	.catch(error => {
		return response.send(error);
	})
}